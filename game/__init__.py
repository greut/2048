"""2048 game in Python (cli or PyQt5)."""

from .game import init_board, round, all_indices, shake, shake_row  # noqa


ROW = 4
"""Size of the board."""
