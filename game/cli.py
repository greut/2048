"""Command-line version of the game."""

import math

from . import ROW, init_board, round, shake


def show(board):
    """Print the 2048 board."""
    print("")
    size = len(board)
    row = int(math.sqrt(size))
    for i in range(0, size, row):
        print("".join(("{0:^5}".format(cell) for cell in board[i:i+row])))
    print("")


def main(board):
    """Play the CLI version of the game."""
    s = "w"
    while s in "wasd":
        show(board)
        s = input("Shake the board with WASD: ").lower()
        board = shake(board, s)
        board = round(board)


if __name__ == "__main__":
    board = init_board(ROW)
    main(board)
