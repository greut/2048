"""2048 game in Python (cli or PyQt5)."""

import sys

from . import ROW, init_board


if __name__ == "__main__":
    board = init_board(ROW)
    try:
        import PyQt5  # noqa
        from .qt import main
        main(board)
    except ImportError:
        try:
            import PyQt4  # noqa
            from .qt import main
            main(board)
        except ImportError:
            print("Install PyQt4 or PyQt5 to play a colorful version of 2048.",
                  file=sys.stderr)

        from .cli import main
        main(board)
