"""Game logic."""

import math

from random import choice


def init_board(size=4):
    """Create a board."""
    return round(round([0] * size**2))


def round(board):
    """
    Insert a 2 at a random available position (where a 0 sits).

    Return a new board.
    """
    index = choice(tuple(all_indices(board, 0)))
    return tuple(board[:index]) + (2,) + tuple(board[index+1:])


def shake(board, direction, max=2048):
    """
    Shake the board in a given direction in wasd.

    >>> shake((0, 2, 2, 0), 'A')
    (2, 0, 2, 0)
    >>> shake((0, 2, 0, 2), 's')
    (0, 0, 0, 4)

    Return a new board.
    """
    direction = direction.lower()
    if direction not in 'wasd':
        raise ValueError("{} does nothing".format(direction))

    rows = list(board)
    row = int(math.sqrt(len(board)))

    for i in range(row):
        if direction == 'a':
            start = i * row
            end = start + row
            rows[start:end] = shake_row(board[start:end], max)

        elif direction == 'd':
            start = i * row - 1
            end = start + row
            # -1 is the last element and not the one before the first one.
            # None must be used instead.
            if start == -1:
                start = None
            rows[end:start:-1] = shake_row(board[end:start:-1], max)

        elif direction in 'w':
            rows[i::row] = shake_row(board[i::row], max)

        elif direction in 's':
            start = (row - 1) * row + i
            rows[start::-row] = shake_row(board[start::-row], max)
    return tuple(rows)


def shake_row(row, max=2048):
    """
    Shake a given row towards its beginning.

    >>> shake_row((0, 2, 0, 2))
    (4, 0, 0, 0)
    >>> shake_row((2, 2, 2, 2))
    (8, 0, 0, 0)
    >>> shake_row((2, 2, 2, 2), max=4)
    (4, 4, 0, 0)
    >>> shake_row((2, 4, 2, 0))
    (2, 4, 2, 0)

    Return a new row.
    """
    l = len(row)
    row = [i for i in row if i != 0]
    m = 2
    while m < max:
        indices = tuple(all_indices(row, m))
        n = m*2
        for i in indices[:-1]:
            if row[i] == row[i+1] == m:
                row[i] = n
                row[i+1] = None
        row = [i for i in row if i is not None]
        m = n

    row.extend([0] * (l - len(row)))
    return tuple(row)


def all_indices(haystack, needle):
    """Like list.index but for all the indices."""
    try:
        indice = -1
        while True:
            indice = haystack.index(needle, indice+1)
            yield indice
    except ValueError:
        pass
