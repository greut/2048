"""Test case for the round function."""

from game import round


def test_round():
    """A round adds a `2' randomly in the board."""
    board = [0] * 8
    new_board = round(board)

    assert board.count(2) == 0, "Original board is not altered."
    assert new_board.count(2) == 1, "New board has one two."


def tes_all_the_rounds():
    """It fills all the places after `size of the board' rounds."""
    board = [0] * 8
    for _ in range(8):
        board = round(board)

    assert board.count(2) == 8, "All the 2 have be filled."
