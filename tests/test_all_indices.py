"""Test cases for the all_indicies method."""

import unittest

from game import all_indices


class TestAllIndices(unittest.TestCase):
    """All indices common tests."""

    def setUp(self):
        """Create a board."""
        self.board = [0] * 8

    def test_all_indices_on_filled_board(self):
        """Test that the `2's' are found."""
        self.board[3] = 2
        self.board[7] = 2
        self.assertEquals([3, 7],
                          list(all_indices(self.board, 2)))
